﻿using System;
using System.Runtime.InteropServices;
using Xunit;
using PopCount;

namespace PopCount.Test
{
    public class Tests
    {
        [Fact]
        public void CanCallPopCount()
        {
            Program.PopCount(1);
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 1)]
        [InlineData(2, 1)]
        [InlineData(3, 2)]
        [InlineData(4, 1)]
        [InlineData(5, 2)]
        [InlineData(8, 1)]
        [InlineData(15, 4)]
        [InlineData(19, 3)]
        public void Should_return_right_result(int number, int expected)
        {
            var result = Program.PopCount(number);
            
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 1)]
        [InlineData(2, 1)]
        [InlineData(3, 2)]
        [InlineData(4, 1)]
        [InlineData(5, 2)]
        [InlineData(8, 1)]
        [InlineData(15, 4)]
        [InlineData(19, 3)]
        public void Alternative_implementation_should_return_right_result(int number, int expected)
        {
            var result = Program.PopCount2(number);
            
            Assert.Equal(expected, result);
        }
    }
}