﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PopCount
{
    public class Program
    {
        public static void Main(string[] args)
        {
            List<int> values = new List<int>() { 0, 5, 8, 15, 19 };
            
            Console.WriteLine("PopCount");
            Console.WriteLine("----------");
            
            foreach (var value in values)
            {
                Console.WriteLine($"population_count({value}) = {PopCount(value)}");
            }
            
            foreach (var value in values)
            {
                Console.WriteLine($"alt population_count({value}) = {PopCount2(value)}");
            }
            
            Console.WriteLine("----------");
            Console.WriteLine("Finis!");
            Console.ReadKey();
        }

        // This reflects my first(ish) thought - but its not quite as nice as I would like
        // because the behaviour of BitArray is slightly more involved than I expected (always learning)
        public static int PopCount(int i)
        {
            var bits = new BitArray(new int[] {i});
            var count = bits.Cast<bool>().Count(x => x);
            return count;
        }
        
        // This is my other thought - a more classical solution - could recurse...
        public static int PopCount2(int i)
        {
            var count = 0;
            for (int remainder = i; remainder > 0; remainder = remainder >> 1)
            {
                count += remainder & 1;
            }

            return count;
        }
    }
}